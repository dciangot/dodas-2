# Welcome to DODAS-2
Given the current variety of tools to manage and deploy container based infrastructure the original mission of DODAS has evolved toward the provisioning of custom analysis facilities on top of either Docker containers or k8s managed clusters. Assuming both of them as already created with any of the available tools of choice. 

## Why an analysis facility? 
[simplified figure - main features]
A vast majority of current use cases related to scientific data analysis share a set of common requirements that can be satisfied with a common software stack underneath. This stack is what we target to provide the communities with, and it commonly goes under the generic definition of Analysis Facility (AF).

### Modern IAM management
![](./img/IAM.png)
...

### Visualize and manipulate data interactively
![](./img/dodas-2-interactive.png)
...
-- customize you experience
-- GPU pluggable

### Skim/pre-process raw data on batch systems
![](./img/dodas-2-batch.png)
... pre configured UI ssh accessible etc
-- read external data using your own libraries

### Deploy your own storage backend
If you want to get all the benefits of the IAM authN/Z also for your storage we got you covered with some ready-to-use implementation:
- S3 Minio:
    - use the AWS STS WebIdentity workflow in combination with OpenPolicyAgent to get a fine grained authorization model in addition to S3 scalability. Optional: mount the buckets posix-like with pre-configured RClone client
- XRootD-5 and WebDAV
    - use the XRootD software to get a storage operation authorized based on IAM gruoups or scopes. WebDAV and XRootD protocol supported.

If you need just a shared posix-accessible FS across the cluster, to provision persistence for your work, you can think about using a Longhorn volume, that works great for this purpose.

## Quick start: deploy your own AF in less than 5 mins

### Before starting
We provide you with 2 different quick-start setup, one is a local one focused on getting the feeling of the JupyterHub interactive experience without the burden of bringing up a k8s cluster.
If you are beyond that point you can take a look at the Kubernetes quick-start that make use of Helm charts to bring up your AF in less than 5 minutes.

- [Docker-compose quick-start]()
- [Kubernetes quick-start]()

### Kubernetes
... 

## What if I want to customize my instance

### Custom user images
...
### Custom user UI flavors
... 
### S3 storage policies
... 

## How to contribute
... 

